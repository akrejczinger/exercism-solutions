module TwoFer exposing (twoFer)


twoFer : Maybe String -> String
twoFer name =
    case name of
        Just str -> "One for " ++ str ++ ", one for me."
        Nothing -> "One for you, one for me."
