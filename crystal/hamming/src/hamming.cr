module Hamming
    extend self

    def compute(first, second)
        if first.size != second.size
            raise ArgumentError.new("Strands not of same length")
        end

        (0...first.size).count do |i|
            first[i] != second[i]
        end
    end
end
