module Primes
  def self.sieve(limit : Int32) : Array(Int32)
    # Limit cannot be below 2
    return [] of Int32 if limit < 2

    # TODO how to create an array with nilable ints?
    marked = (2..limit).to_a

    # mark off non-primes by replacing them with zero
    (2..Math.sqrt(limit)).each_with_index do |num, i|
      i += num
      while i < marked.size
        marked[i] = 0
        i += num
      end
    end

    # remove zero entries
    marked.delete(0)
    marked
  end
end
