# Please implement your solution to rna-transcription in this file
module RnaComplement
  extend self

  def of_dna(dna : String)
    dna.tr("CGTA", "GCAU")
  end
end
