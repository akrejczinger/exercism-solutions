# Please implement your solution to gigasecond in this file
module Gigasecond
  extend self

  def from(time : Time)
    time + 1e9.seconds
  end
end
