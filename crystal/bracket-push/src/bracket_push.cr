module Brackets
  MATCHES = {
    ']' => '[',
    ')' => '(',
    '}' => '{',
  }

  def self.are_valid?(input : String) : Bool
    brackets = Deque(Char).new
    input.chars.each do | char |
      case
      when MATCHES.values.includes?(char)
        brackets.push char
      when MATCHES.keys.includes?(char)
        last_bracket = brackets.pop?
        expecting = MATCHES[char]?

        # if unmatched parentheses: return false
        if expecting != last_bracket
          return false
        end
      end
    end

    # all parentheses should be matched and popped, so the deque must be empty
    brackets.size == 0
  end
end
