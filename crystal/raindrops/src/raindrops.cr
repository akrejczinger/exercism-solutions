module Raindrops
  def self.drops(num : Int32) : String
    result = String.build(capacity=15) do |result|
      result << "Pling" if num % 3 == 0
      result << "Plang" if num % 5 == 0
      result << "Plong" if num % 7 == 0

      result << num.to_s if result.empty?
    end
  end
end
