module Bob
  def self.hey(sentence : String) : String
    case
    when silence?(sentence)
      "Fine. Be that way!"
    when is_question?(sentence) && upper?(sentence)
      "Calm down, I know what I'm doing!"
    when is_question?(sentence)
      "Sure."
    when upper?(sentence)
      "Whoa, chill out!"
    else
      "Whatever."
    end
  end

  private def self.upper?(sentence : String) : Bool
    sentence.upcase() == sentence && sentence.chars.any? {|char| char.letter?}
  end

  private def self.is_question?(sentence : String) : Bool
    sentence.ends_with?('?')
  end

  private def self.silence?(sentence : String) : Bool
    sentence.strip == ""
  end
end
