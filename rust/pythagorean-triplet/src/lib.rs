use std::collections::HashSet;
use std::iter::FromIterator;

pub fn find(sum: u32) -> HashSet<[u32; 3]> {
    let sums = possible_sums(sum);
    let mut result = HashSet::<[u32; 3]>::new();

    for item in sums {
        let [x, y, z] = item;
        println!("x {:?}", x);
        println!("y {:?}", y);
        println!("z {:?}", z);
        if x * x + y * y == z * z {
            result.insert([x, y, z]);
        }
    }
    println!("{:?}", result);

    return result
}

/// Caltulate all the possible triplets which sum up to the given number.
///
/// Triplets should always be in increasing value: a < b < c.
fn possible_sums(sum: u32) -> HashSet<[u32; 3]> {
    // TODO: more functional-style solution?
    // TODO: this is very slow, should return some kind of iterator?
    let mut result: HashSet<[u32; 3]> = HashSet::new();
    for first in 1..sum {
        for second in first+1..sum {
            if first > second {
                break;
            }
            for third in second+1..sum {
                if second > third {
                    break;
                } else if first + second + third == sum {
                    result.insert([first, second, third]);
                }
            }
        }
    }

    return result
}
