pub fn reply(message: &str) -> &str {
    let trimmed = message.trim();
    let last = trimmed.chars().last().unwrap_or(' ');

    match trimmed {
        m if m == "" => "Fine. Be that way!",
        m if is_shouty(m) && last == '?' => "Calm down, I know what I'm doing!",
        m if is_shouty(m) => "Whoa, chill out!",
        _m if last == '?' => "Sure.",
        _ => "Whatever."
    }
}

fn is_shouty(message: &str) -> bool {
    return message == message.to_uppercase() && message != message.to_lowercase()
}
