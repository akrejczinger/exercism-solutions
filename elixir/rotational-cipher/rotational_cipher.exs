defmodule RotationalCipher do
  @doc """
  Given a plaintext and amount to shift by, return a rotated string.

  Example:
  iex> RotationalCipher.rotate("Attack at dawn", 13)
  "Nggnpx ng qnja"
  """
  def rotate_letter(letter, shift) do
    cond do
      (?A <= letter && letter <= ?Z) ->
        ?A + rem(letter + shift - ?A, 26)
      (?a <= letter && letter <= ?z) ->
        ?a + rem(letter + shift - ?a, 26)
      true ->
        letter
    end
  end
  @spec rotate(text :: String.t(), shift :: integer) :: String.t()
  def rotate(text, shift) do
    text
      |> to_charlist
      |> Enum.map(fn(x) -> rotate_letter(x, shift) end)
      |> to_string
  end
end
