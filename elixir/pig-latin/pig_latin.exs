defmodule PigLatin do
  @consonant_groups ["ch", "qu", "squ", "thr", "th", "sch"]
  @vowel_groups ["yt", "xr"]
  @doc """
  Given a `phrase`, translate it a word at a time to Pig Latin.

  Words beginning with consonants should have the consonant moved to the end of
  the word, followed by "ay".

  Words beginning with vowels (aeiou) should have "ay" added to the end of the
  word.

  Some groups of letters are treated like consonants, including "ch", "qu",
  "squ", "th", "thr", and "sch".

  Some groups are treated like vowels, including "yt" and "xr".
  """
  @spec translate(phrase :: String.t()) :: String.t()
  def translate(phrase) do
    phrase
      |> String.split
      |> Enum.map_join(" ", &translate_word/1)
  end

  def translate_word(word) do
    # Get first matching consonant group or nil if no match
    cons_prefix = first_matching(@consonant_groups, word)
    # Get first matching vowel group or nil if no match
    vowel_prefix = first_matching(@vowel_groups, word)

    cond do
      cons_prefix != nil ->
        [rest] = String.split(word, cons_prefix, trim: true)
        rest <> cons_prefix <> "ay"
      vowel_prefix != nil ->
        word <> "ay"
      List.first(to_charlist(word)) in 'aeiou' ->
        word <> "ay"
      true ->
        {first, rest} = String.split_at(word, 1)
        rest <> first <> "ay"
    end
  end

  defp first_matching(groups, word) do
    groups
      |> Enum.filter(fn(prefix) -> String.starts_with?(word, prefix) end)
      |> List.first
  end
end
