defmodule Words do
  @doc """
  Count the number of words in the sentence.

  Words are compared case-insensitively.
  """
  @spec count(String.t()) :: map
  def count(sentence) do
    sentence
      |> String.downcase
      |> String.split(~r/[ _~!@#$%^&;:.,]/, trim: true)
      |> Enum.map_reduce(%{}, fn word, count_map ->
        {word, Map.update(count_map, word, 1, fn x -> x + 1 end)} end)
      |> elem(1)
  end
end
