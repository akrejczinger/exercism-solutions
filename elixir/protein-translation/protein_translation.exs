defmodule ProteinTranslation do
  @codon_to_protein %{
    "UGU" => "Cysteine",
    "UGU" => "Cysteine",
    "UGC" => "Cysteine",
    "UUA" => "Leucine",
    "UUG" => "Leucine",
    "AUG" => "Methionine",
    "UUU" => "Phenylalanine",
    "UUC" => "Phenylalanine",
    "UCU" => "Serine",
    "UCC" => "Serine",
    "UCA" => "Serine",
    "UCG" => "Serine",
    "UGG" => "Tryptophan",
    "UAU" => "Tyrosine",
    "UAC" => "Tyrosine",
    "UAA" => "STOP",
    "UAG" => "STOP",
    "UGA" => "STOP",
  }

  @doc """
  Unwrap a result tuple: return nil if error happened, the wrapped result otherwise
  """
  defp unwrap(codon_result) do
    case codon_result do
      { :error, _ } -> nil
      { :ok, value } -> value
      _ -> nil
    end
  end

  @doc """
  Given an RNA string, return a list of proteins specified by codons, in order.
  """
  @spec of_rna(String.t()) :: { atom,  list(String.t()) }
  def of_rna(rna) do
    codons = to_charlist(rna)
      |> Enum.chunk(3)
      |> Enum.map(&to_string/1)
      |> Enum.map(&of_codon/1)
      |> Enum.map(&unwrap/1)

    if Enum.member?(codons, nil) do
      # If there is a nil, it indicates an error.
      { :error, "invalid RNA" }
    else
      # If no nil, then chop off at the first STOP.
      { :ok, Enum.take_while(codons, fn(x) -> x != "STOP" end) }
    end
  end

  @doc """
  Given a codon, return the corresponding protein

  UGU -> Cysteine
  UGC -> Cysteine
  UUA -> Leucine
  UUG -> Leucine
  AUG -> Methionine
  UUU -> Phenylalanine
  UUC -> Phenylalanine
  UCU -> Serine
  UCC -> Serine
  UCA -> Serine
  UCG -> Serine
  UGG -> Tryptophan
  UAU -> Tyrosine
  UAC -> Tyrosine
  UAA -> STOP
  UAG -> STOP
  UGA -> STOP
  """
  @spec of_codon(String.t()) :: { atom, String.t() }
  def of_codon(codon) do
    translated = Map.get(@codon_to_protein, codon)

    if translated == nil do
      { :error, "invalid codon" }
    else
      { :ok, translated }
    end
  end
end
